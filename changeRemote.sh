remoteURL=`git config --get remote.origin.url`
lenURL=`expr length $remoteURL`

isSetToHome=${remoteURL:0:3}

if [ "$isSetToHome" = "ssh" ]; then
	classproject=`echo $remoteURL | cut -d '/' -f 5`
	repo=${remoteURL:28:lenURL-28}
	group=`echo $remoteURL | cut -d '/' -f 6`
else
	classproject=`echo $remoteURL | cut -d '/' -f 2`
	repo=${remoteURL:8:lenURL-8}
	group=`echo $remoteURL | cut -d '/' -f 3`
fi

class=${classproject:0:3}
len=`expr length $classproject`
project=${classproject:3:$len-3}
group=${group:6:1}

echo
echo -e "Class--------" $class
echo -e "Project------" $project
echo -e "Group--------" $group
echo

if [ "$isSetToHome" = "ssh" ]; then
	echo This project is actually set to HOME
else
	echo This project is actually set to SCHOOL
fi

echo "Do you want to change it ? (y/n)"
read reply
	
if [ "$reply" = "y" ]; then
	if [ "$isSetToHome" = "ssh" ]; then
		newRemote="git@git:$repo"
		echo Change it to SCHOOL
	else
		newRemote="ssh://git@progisart.fr:2424/$repo"
		echo Change it to HOME
	fi
	`git remote set-url origin $newRemote`
else
	echo "Ok goodbye"
fi