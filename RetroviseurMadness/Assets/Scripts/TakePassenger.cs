﻿using UnityEngine;
using System.Collections;

public class TakePassenger : MonoBehaviour {

    public Collider col;

    void Awake()
    {
        col = null;
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "WaitingCharacter")
            col = collider;
    }

    void OnTriggerExit(Collider collider)
    {
        if (col == collider)
            col = null;
    }

    public Collider GetCollider()
    {
        Collider tmp = col;
        col = null;
        return tmp;
    }
}
