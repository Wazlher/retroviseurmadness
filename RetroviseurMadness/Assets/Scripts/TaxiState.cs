﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Vehicles.Car;
using System.Collections.Generic;

public class TaxiState : MonoBehaviour
{

    public enum E_TaxiState
    {
        E_INTRO,
        E_STARTING,
        E_DRIVING,
        E_RETURNED_PICKING,
        E_RETURNED_ACTING,
        E_STOPPED,
    }

    public float StartGameSecondAvailaible;
    float timeRemaining;
    bool isEnd;
    int dollars;
    public float intervalWinMoney;
    private float timeWinMoney;

    E_TaxiState taxiState;
    CameraManager camManager;
    CarControl carControl;
    CarAudio carAudio;
    TakePassenger colliderNewPassenger;
    GUIManager guiManager;

    CharacterEntity leftPassenger;
    CharacterEntity middlePassenger;
    CharacterEntity rightPassenger;
    public Transform leftPosition;
    public Transform middlePosition;
    public Transform rightPosition;
    CharacterEntity chosenCharacter;

    public AudioClip gameoverSound;
    public Animation leftEject;
    public Animation middleEject;
    public Animation rightEject;

    private float soundTimeTreshold;
    private float soundTime;

    public GameObject car;

    void Awake()
    {
        camManager = (CameraManager)GameObject.FindGameObjectWithTag("CameraManager").GetComponent<CameraManager>();
        taxiState = E_TaxiState.E_STARTING;
        carControl = (CarControl)GameObject.Find("Car").GetComponent<CarControl>();
        carAudio = (CarAudio)GameObject.Find("Car").GetComponent<CarAudio>();
        colliderNewPassenger = (TakePassenger)GameObject.Find("ColliderTakeNewPassenger").GetComponent<TakePassenger>();
        guiManager = (GUIManager)GameObject.FindGameObjectWithTag("GUIManager").GetComponent<GUIManager>();

        leftPassenger = null;
        middlePassenger = null;
        rightPassenger = null;
        chosenCharacter = null;

        isEnd = false;
        soundTime = Time.time;
        soundTimeTreshold = 2.0f;
        dollars = 0;
        timeWinMoney = 0.0f;
    }

    void Start()
    {
        carAudio.enabled = false;
        GetComponent<AudioSource>().Play();
    }

    void Update()
    {
        if (isEnd == false)
        {
            UpdateInput();
            UpdateSound();
        }
        else if (isEnd == false)
        {
            Debug.Log("Game is ended");
        }
    }

    private void UpdateInput()
    {
        if (leftPassenger != null)
        {
            if (middlePassenger != null)
            {
                if (rightPassenger != null)
                    guiManager.UpdateBoardControl(timeRemaining, dollars, leftPassenger.GetAngriness(), leftPassenger.GetMaxAnger(), middlePassenger.GetAngriness(), middlePassenger.GetMaxAnger(), rightPassenger.GetAngriness(), rightPassenger.GetMaxAnger());
                else
                    guiManager.UpdateBoardControl(timeRemaining, dollars, leftPassenger.GetAngriness(), leftPassenger.GetMaxAnger(), middlePassenger.GetAngriness(), middlePassenger.GetMaxAnger(), -1, -1);
            }
            else
                guiManager.UpdateBoardControl(timeRemaining, dollars, leftPassenger.GetAngriness(), leftPassenger.GetMaxAnger(), -1, -1, -1, -1);
        }
        else
            guiManager.UpdateBoardControl(timeRemaining, dollars, -1, -1, -1, -1, -1, -1);

        if (timeWinMoney > intervalWinMoney)
        {
            if (leftPassenger != null)
                dollars += leftPassenger.GetMoney();
            if (middlePassenger != null)
                dollars += middlePassenger.GetMoney();
            if (rightPassenger != null)
                dollars += rightPassenger.GetMoney();
            timeWinMoney = 0.0f;
        }

        if (taxiState != E_TaxiState.E_STARTING)
        {
            timeRemaining -= Time.deltaTime;
            timeWinMoney += Time.deltaTime;
            if (timeRemaining < 0.0f)
            {
                isEnd = true;
                carControl.Brake();
                guiManager.EnableTex(1, true);
                Invoke("EndGame", 17.0f);
                GetComponent<AudioSource>().PlayOneShot(gameoverSound);
            }
        }

        if (taxiState == E_TaxiState.E_STARTING && GetComponent<AudioSource>().isPlaying == false)
        {
            timeRemaining = StartGameSecondAvailaible;
            carAudio.enabled = true;
            taxiState = E_TaxiState.E_DRIVING;
            carControl.Accelerate();
        }
        else if (CrossPlatformInputManager.GetButtonDown("SeRetourner") == true)
        {
            if (taxiState == E_TaxiState.E_DRIVING)
            {
                taxiState = E_TaxiState.E_RETURNED_PICKING;
                camManager.ChangeCamera(CameraManager.E_Camera.E_PASSENGERS);
                StartCoroutine(guiManager.WaitEnableGUINewPassenger(1.0f, true));
                guiManager.EnableGUIBoardControl(false);
            }
            else if (taxiState == E_TaxiState.E_RETURNED_PICKING || taxiState == E_TaxiState.E_RETURNED_ACTING)
            {
                chosenCharacter = null;
                taxiState = E_TaxiState.E_DRIVING;
                camManager.ChangeCamera(CameraManager.E_Camera.E_DRIVING);
                guiManager.EnableGUIChosePassenger(false);
                StartCoroutine(guiManager.WaitEnableGUIBoardControl(1.0f, true));
            }
        }

        else if (taxiState == E_TaxiState.E_DRIVING && CrossPlatformInputManager.GetButtonDown("Arret"))
        {
            carControl.Brake();
            taxiState = E_TaxiState.E_STOPPED;
            camManager.ChangeCamera(CameraManager.E_Camera.E_NEW_PASSENGER);
            StartCoroutine(guiManager.WaitEnableGUIAcceptPassenger(0.5f, true));
            guiManager.EnableGUIBoardControl(false);
            CharacterEntity chara = CheckNewPassenger();
            if (chara != null)
            {
                chara.MoveToCar();
            }
        }

        else if (taxiState == E_TaxiState.E_STOPPED && CrossPlatformInputManager.GetButtonDown("AccepterPassager"))
        {
            CharacterEntity newPassenger = CheckNewPassenger();
            Debug.Log("New passenger = " + newPassenger);
            if (newPassenger != null)
            {
                Vector3 pos = ChosePassengerPosition(newPassenger);
                if (pos != Vector3.zero)
                {
                    newPassenger.TaxiResponse();
                    newPassenger.MoveOutsideToCar(pos, car);
                    newPassenger.PlayJingle();
                    newPassenger.tag = "Character";
                    carControl.Accelerate();
                    taxiState = E_TaxiState.E_DRIVING;
                    camManager.ChangeCamera(CameraManager.E_Camera.E_DRIVING);
                    StartCoroutine(guiManager.WaitEnableGUIBoardControl(0.5f, true));
                    guiManager.EnableGUIAcceptPassenger(false);
                }
            }
        }

        else if (taxiState == E_TaxiState.E_STOPPED && CrossPlatformInputManager.GetButtonDown("RefuserPassager"))
        {
            CharacterEntity newPassenger = CheckNewPassenger();
            if (newPassenger != null)
            {
                Debug.Log("Refuse");
                newPassenger.TaxiResponse();
                carControl.Accelerate();
                taxiState = E_TaxiState.E_DRIVING;
                camManager.ChangeCamera(CameraManager.E_Camera.E_DRIVING);
                StartCoroutine(guiManager.WaitEnableGUIBoardControl(0.5f, false));
                guiManager.EnableGUIAcceptPassenger(false);
            }
        }


        else if (taxiState == E_TaxiState.E_RETURNED_PICKING && CrossPlatformInputManager.GetButtonDown("PassagerGauche") && leftPassenger != null)
        {
            taxiState = E_TaxiState.E_RETURNED_ACTING;
            chosenCharacter = leftPassenger;
            guiManager.EnableGUIChosePassenger(false);
            guiManager.EnableGUIPActionassenger(true, "left");
        }
        else if (taxiState == E_TaxiState.E_RETURNED_PICKING && CrossPlatformInputManager.GetButtonDown("PassagerMilieu") && middlePassenger != null)
        {
            taxiState = E_TaxiState.E_RETURNED_ACTING;
            chosenCharacter = middlePassenger;
            guiManager.EnableGUIChosePassenger(false);
            guiManager.EnableGUIPActionassenger(true, "middle");
        }
        else if (taxiState == E_TaxiState.E_RETURNED_PICKING && CrossPlatformInputManager.GetButtonDown("PassagerDroite") && rightPassenger != null)
        {
            taxiState = E_TaxiState.E_RETURNED_ACTING;
            chosenCharacter = rightPassenger;
            guiManager.EnableGUIChosePassenger(false);
            guiManager.EnableGUIPActionassenger(true, "right");
        }

        else if (taxiState == E_TaxiState.E_RETURNED_ACTING && CrossPlatformInputManager.GetButtonDown("PassagerGauche")) // RAISONER
        {
            Debug.Log("TaxiState Raisonner");
            chosenCharacter.ResponseRaisonner();
            chosenCharacter = null;
            guiManager.EnableGUIPActionassenger(false, "");
            Invoke("WaitUntilEjectionAnimationEnd", 1.0f);
        }
        else if (taxiState == E_TaxiState.E_RETURNED_ACTING && CrossPlatformInputManager.GetButtonDown("PassagerMilieu")) // ENGUEULER
        {
            Debug.Log("TaxiState Gueuler");
            chosenCharacter.ResponseGueuler();
            chosenCharacter = null;
            guiManager.EnableGUIPActionassenger(false, "");
            Invoke("WaitUntilEjectionAnimationEnd", 1.0f);
        }
        else if (taxiState == E_TaxiState.E_RETURNED_ACTING && CrossPlatformInputManager.GetButtonDown("PassagerDroite")) // CLAQUER
        {
            Debug.Log("TaxiState Claquer");
            chosenCharacter.ResponseClaque();
            chosenCharacter = null;
            guiManager.EnableGUIPActionassenger(false, "");
            Invoke("WaitUntilEjectionAnimationEnd", 1.0f);
        }
        else if (taxiState == E_TaxiState.E_RETURNED_ACTING && CrossPlatformInputManager.GetButtonDown("Ejecter"))
        {
            Debug.Log("TaxiState Ejecter");
            chosenCharacter.Ejecter();
            if (leftPassenger == chosenCharacter)
            {
                leftEject.Play();
                leftPassenger = null;
            }
            else if (middlePassenger == chosenCharacter)
            {
                middleEject.Play();
                middlePassenger = null;
            }
            else if (rightPassenger == chosenCharacter)
            {
                rightEject.Play();
                rightPassenger = null;
            }
            chosenCharacter = null;
            guiManager.EnableGUIPActionassenger(false, "");
            Invoke("WaitUntilEjectionAnimationEnd", 0.75f);
        }

        else if (taxiState == E_TaxiState.E_STOPPED && CrossPlatformInputManager.GetButtonDown("Arret"))
        {
            carControl.Accelerate();
            taxiState = E_TaxiState.E_DRIVING;
            camManager.ChangeCamera(CameraManager.E_Camera.E_DRIVING);
            guiManager.EnableGUIAcceptPassenger(false);
            StartCoroutine(guiManager.WaitEnableGUIBoardControl(0.5f, true));
        }
    }

    void WaitUntilEjectionAnimationEnd()
    {
        taxiState = E_TaxiState.E_DRIVING;
        camManager.ChangeCamera(CameraManager.E_Camera.E_DRIVING);
        StartCoroutine(guiManager.WaitEnableGUIBoardControl(1.0f, true));
    }

    private void UpdateSound()
    {
        /*  if (Time.time > soundTime)
          {
              List<int> list = new List<int>{1, 2, 3};
              CharacterEntity chara = null;
              while (list.Count != 0)
              {
                  int rnd = Random.Range(0, list.Count - 1);
                  if (list[rnd] == 1)
                      chara = leftPassenger;
                  else if (list[rnd] == 2)
                      chara = middlePassenger;
                  else if (list[rnd] == 3)
                      chara = rightPassenger;
                  if (chara != null && chara.GetCharacterState() == CharacterEntity.E_CharacterState.E_Idle)
                      break;
                  else
                  {
                      list.Remove(rnd);
                      chara = null;
                  }
                  Debug.Log("rnd = " + rnd + " count = " + list.Count);
              }
              if (chara != null)
                  Debug.Log(chara + " can play a sound");
              soundTime += soundTimeTreshold;
          }*/
    }

    CharacterEntity CheckNewPassenger()
    {
        Collider col = colliderNewPassenger.GetCollider();
        if (col != null)
        {
            CharacterEntity chara = (CharacterEntity)(col.gameObject.GetComponent<CharacterEntity>());
            return chara;
        }
        return null;
    }

    Vector3 ChosePassengerPosition(CharacterEntity chara)
    {
        if (leftPassenger == null)
        {
            leftPassenger = chara;
            return leftPosition.position;
        }
        else if (middlePassenger == null)
        {
            middlePassenger = chara;
            return middlePosition.position;
        }
        else if (rightPassenger == null)
        {
            rightPassenger = chara;
            return rightPosition.position;
        }
        return Vector3.zero;
    }

    public void AddTime(float timeToAdd)
    {
        if (isEnd == false)
            timeRemaining += timeToAdd;
    }

    public bool GetIsEnd()
    {
        return isEnd;
    }

    public void CallPassenger()
    {
        CharacterEntity chara = CheckNewPassenger();
        if (chara != null)
            chara.MoveToCar();
    }

    public void Collision()
    {
        Debug.Log("COLLISION WHEEL");
    }

    private void EndGame()
    {
        Application.LoadLevel("Menu");
    }
}
