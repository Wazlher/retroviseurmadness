﻿using UnityEngine;
using System.Collections;

public class SeeCharacter : MonoBehaviour {

    GUIManager guiManager;

    void Awake()
    {
        guiManager = (GUIManager)GameObject.FindGameObjectWithTag("GUIManager").GetComponent<GUIManager>();
    }

	void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "WaitingCharacter")
        {
            guiManager.EnableGUINewPassenger(true);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "WaitingCharacter")
        {
            guiManager.EnableGUINewPassenger(false);
        }
    }
}
