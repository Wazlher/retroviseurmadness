﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

    public GameObject[] StopNewPassenger;
    public GameObject[] AcceptPassenger;
    public GameObject[] ChosePassenger;
    public GameObject[] ActionPassenger;
    public Vector3 leftAction;
    public Vector3 middleAction;
    public Vector3 rightAction;
    public GameObject dollars;
    public GameObject time;
    public GameObject leftP;
    public GameObject middleP;
    public GameObject rightP;
    public GameObject checkpointTex;
    public GameObject timesupTex;

    private bool boardControlVisible;
    private bool showTex;
    private float timeTex;

    void Awake()
    {
        boardControlVisible = true;
        showTex = false;
        timeTex = 0.0f;
    }
    public void EnableGUINewPassenger(bool enable)
    {
        foreach (var gui in StopNewPassenger)
            gui.SetActive(enable);
    }

    public IEnumerator WaitEnableGUINewPassenger(float waitTime, bool enable)
    {
        yield return new WaitForSeconds(waitTime);
        EnableGUIChosePassenger(enable);
    }


    public void EnableGUIChosePassenger(bool enable)
    {
        foreach (var gui in ChosePassenger)
            gui.SetActive(enable);
    }

    public IEnumerator WaitEnableGUIAcceptPassenger(float waitTime, bool enable)
    {
        yield return new WaitForSeconds(waitTime);
        EnableGUIAcceptPassenger(enable);
    }

    public void EnableGUIAcceptPassenger(bool enable)
    {
        foreach (var gui in AcceptPassenger)
            gui.SetActive(enable);
    }

    public void EnableGUIPActionassenger(bool enable, string pos)
    {
        foreach (var gui in ActionPassenger)
        {
            if (pos == "left")
                gui.GetComponent<RectTransform>().position = leftAction;
            if (pos == "middle")
                gui.GetComponent<RectTransform>().position = middleAction;
            if (pos == "right")
                gui.GetComponent<RectTransform>().position = rightAction;

            gui.SetActive(enable);
        }
    }

    public void EnableGUIBoardControl(bool enable)
    {
        dollars.SetActive(enable);
        time.SetActive(enable);
        leftP.SetActive(enable);
        middleP.SetActive(enable);
        rightP.SetActive(enable);
        boardControlVisible = enable;
    }
    public IEnumerator WaitEnableGUIBoardControl(float waitTime, bool enable)
    {
        yield return new WaitForSeconds(waitTime);
        EnableGUIBoardControl(enable);
    }
    public void UpdateBoardControl(float time, int dollars, float value1, float max1, float value2, float max2, float value3, float max3)
    {
        if (boardControlVisible == false)
            return;

        this.dollars.GetComponent<Text>().text = dollars.ToString();
        this.time.GetComponent<Text>().text = Math.Round(time).ToString();
        if (BoardControlColorBar(leftP.GetComponent<Image>(), value1, max1) == true)
            leftP.SetActive(true);
        else
            leftP.SetActive(false);
        if (BoardControlColorBar(middleP.GetComponent<Image>(), value2, max2) == true)
            middleP.SetActive(true);
        else
            middleP.SetActive(false);
        if (BoardControlColorBar(rightP.GetComponent<Image>(), value3, max3) == true)
            rightP.SetActive(true);
        else
            rightP.SetActive(false);
    }

    private bool BoardControlColorBar(Image img, float value, float max)
    {
        if (value == -1)
            return false;
        Debug.Log("GUIM = " + value);
        float percent = value * 100 / max;
        if (percent < 33.3f)
        {
            img.material.color = Color.green;
            img.color = Color.green;
        }
        else if (percent >= 33.3f && percent < 66.6f)
        {
            img.material.color = Color.yellow;
            img.color = Color.yellow;
        }
        else if (percent >= 66.6f)
        {
            img.material.color = Color.red;
            img.color = Color.red;
        }
        return true;
    }

    public void EnableTex(int tex, bool enable)
    {
        showTex = true;
        if (tex == 0)
            checkpointTex.SetActive(enable);
        else if (tex == 1)
            timesupTex.SetActive(enable);

    }

    void Update()
    {
        if (showTex)
        {
            timeTex += Time.deltaTime;
            if (timeTex >= 3.0f)
            {
                showTex = false;
                timeTex = 0.0f;
                EnableTex(0, false);
                EnableTex(1, false);
            }
        }
    }
}
