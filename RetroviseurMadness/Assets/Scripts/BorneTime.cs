﻿using UnityEngine;
using System.Collections;

public class BorneTime : MonoBehaviour {

    public float AddedSeconds;
	public AudioClip TimeAdded;

	void OnTriggerEnter(Collider col)
    {
        if (this.enabled && col.tag == "CarCollider")
        {
            this.enabled = false;
            TaxiState taxi = (TaxiState)GameObject.FindGameObjectWithTag("TaxiState").GetComponent<TaxiState>();
            taxi.AddTime(AddedSeconds);
			AudioSource.PlayClipAtPoint(TimeAdded,Vector3.zero);
            GUIManager guiManager = (GUIManager)GameObject.FindGameObjectWithTag("GUIManager").GetComponent<GUIManager>();
            guiManager.EnableTex(0, true);
        }
    }

}
