﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuScript : MonoBehaviour {

	public Canvas StartMenu;
	public Canvas quitmenu; 
	public Button startText;
	public Button exitText;
	private float delay;
	private bool startdelay; 
	private float alphaForText;
	public CanvasGroup CanvaAlphaMadaFaKa;
	private float Speed;
	private float AlphaForBackground;
	public CanvasGroup Background;

	// Use this for initialization
	void Start () 
	{
	
		quitmenu = quitmenu.GetComponent<Canvas> ();
		startText = startText.GetComponent<Button> (); 
		exitText = exitText.GetComponent<Button> ();
		quitmenu.enabled = false;
		delay = 0.0f; 
		alphaForText = 1.0f;
		Speed = 2.0f;
	}

	public void ExitPress()
	{
		Application.Quit ();
	}

	public void NoPress()
	{
		quitmenu.enabled = false;
		startText.enabled = true; 
		exitText.enabled = true;
	}
	
	public void StartLevel()
	{
		startdelay = true;
		startText.enabled = false; 
		exitText.enabled = false;
	}

	public void exitGame()
	{
		Application.Quit ();
	}
	// Update is called once per frame
	void Update () 
	{
		CanvaAlphaMadaFaKa.alpha = alphaForText;
		Background.alpha = AlphaForBackground;

		if (startdelay == true) 
		{
			delay += Time.deltaTime; 
			alphaForText -= (Time.deltaTime * Speed); 
			AlphaForBackground += (Time.deltaTime * Speed);
		}

		if (delay >= 1.5f) 
		{
			Application.LoadLevel (1);
		}
	}
}
