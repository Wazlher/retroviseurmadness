﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadGeneration : MonoBehaviour {
	
    [System.Serializable]
    public struct DicCharacter
    {
        public E_Character type;
        public GameObject prefab;
    }

	public GameObject[]		roads;
	private int				nbSegmentAvailable;
	private Vector3			lastPosition;
    public float distanceBetweenRoads;
    public int ratioCharacterAppearance;
   // public int ratioBorneAppearance;
    public DicCharacter[] dicCharacter;

	private List<Object> oldRoads;
    private int id;
	
	void Awake()
	{
		nbSegmentAvailable = 0;
		lastPosition = new Vector3 (0.0f, 0.0f, distanceBetweenRoads * 10.0f);
        oldRoads = new List<Object>();
        id = 0;
	}

	void Start()
	{
		GenerateRoadSegments (30, false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (nbSegmentAvailable < 20)
			GenerateRoadSegments(10, true);
	}

	void GenerateRoadSegments(int nb, bool removeOld)
	{
		int roadGenerated;
		for (roadGenerated = 0; roadGenerated < nb; roadGenerated++)
		{
			int randomRoad = Random.Range (0, roads.Length);
            GameObject tmp = GameObject.Instantiate(roads[randomRoad], lastPosition, Quaternion.identity) as GameObject;
			oldRoads.Add (tmp);
            if (removeOld == true)
            {
                Destroy(oldRoads[0]);
                oldRoads.RemoveAt(0);
            }

            int rnd = UnityEngine.Random.Range(0, ratioCharacterAppearance);
            if (rnd == 1 && ratioCharacterAppearance >= 2)
            {
                int rndChara = UnityEngine.Random.Range(0, 7);
                Transform spawnPoint = GetChild(tmp.transform, "SpawnPoint");
                GameObject prefab = dicCharacter[GetIndexCharacter((E_Character)rndChara)].prefab;
                GameObject chara = (GameObject)GameObject.Instantiate(prefab, spawnPoint.position, Quaternion.identity);
            }
            else
            {
                Transform lampadaire = GetChild(tmp.transform, "lampadaire_object");
                lampadaire.gameObject.SetActive(false);
            }
          /* if (id % ratioBorneAppearance == 0)
            {
                Transform lampadaire = GetChild(tmp.transform, "BorneTime");
                lampadaire.gameObject.SetActive(false);
            }*/
			lastPosition.z -= distanceBetweenRoads;
			nbSegmentAvailable++;
            id++;
		}
	}

	public void DecrementNbRoad(int nb)
	{
		nbSegmentAvailable -= nb;
	}

    private int GetIndexCharacter(E_Character type)
    {
        for (int i = 0; i < dicCharacter.Length; ++i)
        {
            if (dicCharacter[i].type == type)
                return i;
        }
        return 0;
    }

    private Transform GetChild(Transform first, string ChildName)
    {
        for (int i = 0; i < first.transform.childCount; ++i)
        {
            Transform t = first.transform.GetChild(i);
            if (t.name.CompareTo(ChildName) == 0)
                return t;
            if (t.childCount > 0)
            {
                Transform c = GetChild(t, ChildName);
                if (c != null)
                    return c;
            }
        }
        return null;
    }
}
