﻿using UnityEngine;
using System.Collections;

public class Maite : CharacterEntity {

	public Maite()
    {
        type = E_Character.E_MAITE;
    }

    public override void ActionFirstStade()
    {
        base.ActionFirstStade();
    }
    public override void ActionSecondStade()
    {
        base.ActionSecondStade();
    }
    public override void ActionThirdStade()
    {
        base.ActionThirdStade();
    }
    public override void ResponseRaisonner()
    {
        base.ResponseRaisonner();
    }
    public override void ResponseGueuler()
    {
        base.ResponseGueuler();
    }
    public override void ResponseClaque()
    {
        base.ResponseClaque();
    }
}
