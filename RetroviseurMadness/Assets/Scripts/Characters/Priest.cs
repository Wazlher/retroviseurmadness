﻿using UnityEngine;
using System.Collections;

public class Priest : CharacterEntity {

    public string idleAttaqueLight;
    public string idleAttaqueFinale;

    private bool Attack1Ok;
    private bool Attack2Ok;

	public Priest()
    {
        type = E_Character.E_PRETRE;
        Attack1Ok = false;
        Attack2Ok = false;
    }

    public override void ActionFirstStade()
    {
        base.ActionFirstStade();
    }
    public override void ActionSecondStade()
    {
        base.ActionSecondStade();
    }
    public override void ActionThirdStade()
    {

        base.ActionThirdStade();
    }
    public override void ResponseRaisonner()
    {
        base.ResponseRaisonner();
    }
    public override void ResponseGueuler()
    {
        base.ResponseGueuler();
    }
    public override void ResponseClaque()
    {
        base.ResponseClaque();
    }
    protected virtual void ChoseNextAnimation()
    {
        if (angrinessStade == E_AngrinessStade.E_FirstStade)
        {
            Attack1Ok = false;
            Attack2Ok = false;
            _animation.PlayQueued(idleInside);
        }
        else if (angrinessStade == E_AngrinessStade.E_SecondStade)
        {
            Attack2Ok = false;
            if (Attack1Ok == false)
            {
                Attack1Ok = true;
                _animation.PlayQueued(attack1);
            }
            _animation.PlayQueued(idleAttaqueLight);
        }
        else if (angrinessStade == E_AngrinessStade.E_ThirdStade)
        {
            if (Attack2Ok == false)
            {
                Attack2Ok = true;
                _animation.PlayQueued(attack2);
            }
            _animation.PlayQueued(idleAttaqueFinale);
        }
    }
}
