﻿using UnityEngine;
using System.Collections;

public class Lama : CharacterEntity {

	public Lama()
    {
        type = E_Character.E_LAMA;
    }

    public override void ActionFirstStade()
    {
        base.ActionFirstStade();
    }
    public override void ActionSecondStade()
    {
        base.ActionSecondStade();
    }
    public override void ActionThirdStade()
    {
        base.ActionThirdStade();
    }
    public override void ResponseRaisonner()
    {
        base.ResponseRaisonner();
    }
    public override void ResponseGueuler()
    {
        base.ResponseGueuler();
    }
    public override void ResponseClaque()
    {
        base.ResponseClaque();
    }
}
