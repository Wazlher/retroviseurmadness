﻿using UnityEngine;
using System.Collections;

public class BusinessMan : CharacterEntity {

	public BusinessMan()
    {
        type = E_Character.E_HOMME_AFFAIRE;
    }

    public override void ActionFirstStade()
    {
        base.ActionFirstStade();
    }
    public override void ActionSecondStade()
    {
        base.ActionSecondStade();
    }
    public override void ActionThirdStade()
    {
        base.ActionThirdStade();
    }
    public override void ResponseRaisonner()
    {
        base.ResponseRaisonner();
    }
    public override void ResponseGueuler()
    {
        base.ResponseGueuler();
    }
    public override void ResponseClaque()
    {
        base.ResponseClaque();
    }
}
