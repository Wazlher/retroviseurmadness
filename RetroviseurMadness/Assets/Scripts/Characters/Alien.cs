﻿using UnityEngine;
using System.Collections;

public class Alien : CharacterEntity {

	public Alien()
    {
        type = E_Character.E_ALIEN;
    }

    public override void ActionFirstStade()
    {
        base.ActionFirstStade();
    }
    public override void ActionSecondStade()
    {
        base.ActionSecondStade();
    }
    public override void ActionThirdStade()
    {
        base.ActionThirdStade();
    }
    public override void ResponseRaisonner()
    {
        base.ResponseRaisonner();
    }
    public override void ResponseGueuler()
    {
        base.ResponseGueuler();
    }
    public override void ResponseClaque()
    {
        base.ResponseClaque();
    }
}
