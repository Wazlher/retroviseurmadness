﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;
using System;

public abstract class CharacterEntity : MonoBehaviour
{

    public enum E_AngrinessStade
    {
        E_FirstStade,
        E_SecondStade,
        E_ThirdStade
    }

    public enum E_CharacterState
    {
        E_Idle,
        E_Jingle,
        E_Random,
        E_FirstAct,
        E_SecondAct,
        E_Raisoner,
        E_Gifle,
        E_Gueler,
        E_ThirdAct
    }

    protected CharacterConf characterConf;
    private Character characterInfo;
    protected E_Character type;
    protected E_CharacterState state;
    protected int angriness; /* between 0 and value in CharacterConf */
    protected E_AngrinessStade angrinessStade;
    protected AudioSource audioSource;
    public float moveSpeed;
    public float ejectSpeed;
    protected float timeAddAnger;
    protected float timeRandom;

    public AudioClip jingle;
    public AudioClip[] random;
    public AudioClip attackLight;
    public AudioClip attackMedium;
    public AudioClip attackHard;
    public AudioClip responseGifle;
    public AudioClip responseEjecter;
    public AudioClip[] randomSlap;
    public AudioClip gueuler;
    public AudioClip raisoner;

    public Animation _animation;
    public string idleOutside;
    public string idleInside;
    public string attack1;
    public string attack2;
    public string claque;
    public string engueuler;
    public string raisonner;

    private GameObject fxPos;
    public GameObject fx;
    private GameObject instanceFx;

    bool isMovingToCar;
    bool isInCar;
    bool isEjecting;

    void Awake()
    {
        characterConf = (CharacterConf)GameObject.FindGameObjectWithTag("CharacterConf").GetComponent<CharacterConf>();
        if (characterConf == null)
            Debug.Log("Chara conf = " + characterConf);
        characterInfo = characterConf.GetCharacterConf(E_Character.E_JAPONIAISE);
        audioSource = gameObject.AddComponent<AudioSource>();
        angriness = 0;
        angrinessStade = E_AngrinessStade.E_FirstStade;
        state = E_CharacterState.E_Idle;
        isMovingToCar = false;
        isInCar = false;
        _animation[claque].wrapMode = WrapMode.Once;
        _animation[engueuler].wrapMode = WrapMode.Once;
        _animation[raisonner].wrapMode = WrapMode.Once;

        _animation.Play(idleOutside);

        if (type == E_Character.E_CHICKEN_MYTHIC)
            fxPos = GameObject.FindGameObjectWithTag("ChickenFX");
        else if (type == E_Character.E_JAPONIAISE)
            fxPos = GameObject.FindGameObjectWithTag("JaponaiseFX");
        else if (type == E_Character.E_PRETRE)
            fxPos = GameObject.FindGameObjectWithTag("PriestFX");
        else if (type == E_Character.E_ALIEN)
            fxPos = GameObject.FindGameObjectWithTag("AlienFX");
        else if (type == E_Character.E_MAITE)
            fxPos = GameObject.FindGameObjectWithTag("MaiteFX");
        else if (type == E_Character.E_LAMA)
            fxPos = GameObject.FindGameObjectWithTag("LamaFX");
        else if (type == E_Character.E_HOMME_AFFAIRE)
            fxPos = GameObject.FindGameObjectWithTag("BusinessFX");
        Debug.Log(type + " " + fxPos);
    }

    public void MoveOutsideToCar(Vector3 newPosition, GameObject car)
    {
        isMovingToCar = false;
        this.transform.parent = car.transform;
        this.transform.position = newPosition;
        timeAddAnger = 0.0f;
        timeRandom = 0.0f;
        isInCar = true;
        _animation.Play(idleInside);
    }

    public void MoveToCar()
    {
        isMovingToCar = true;
    }

    public void CheckAddAnger()
    {
        if (timeAddAnger >= 2.0f)
        {
            timeAddAnger = 0.0f;
            angriness += characterInfo.AngerBy5Second;
            if (angriness > characterInfo.maxAngerValue)
                angriness = characterInfo.maxAngerValue;
            UpdateAngrinessStade();
        }
    }

    public IEnumerator QueuedSong(float waitTime, AudioClip clip)
    {
        yield return new WaitForSeconds(waitTime);
        if (isEjecting == false && isMovingToCar == false)
            audioSource.PlayOneShot(clip);
    }

    public void PlayJingle()
    {
        audioSource.PlayOneShot(jingle);
        Invoke("EndSound", jingle.length);
        state = E_CharacterState.E_Jingle;
        StartCoroutine(QueuedSong(jingle.length + 0.8f, attackLight));
    }
    public void PlayRandom()
    {
        int rnd = UnityEngine.Random.Range(0, random.Length - 1);
        audioSource.PlayOneShot(random[rnd]);
        Invoke("EndSound", random[rnd].length);
        state = E_CharacterState.E_Random;
    }
    public E_CharacterState GetCharacterState()
    {
        return state;
    }
    public virtual void ActionFirstStade()
    {
        if (fx != null && instanceFx != null)
            instanceFx.SetActive(false);
        audioSource.PlayOneShot(attackLight);
        _animation.Play(idleInside);
        Invoke("EndSound", attackLight.length);
        state = E_CharacterState.E_FirstAct;
    }
    public virtual void ActionSecondStade()
    {
        if (fx != null && instanceFx != null)
            instanceFx.SetActive(false);
        audioSource.PlayOneShot(attackMedium);
        _animation.Play(attack1);
        Invoke("EndSound", attackMedium.length);
        state = E_CharacterState.E_SecondAct;
    }
    public virtual void ActionThirdStade()
    {
        Debug.Log("THIRD STADE");
        if (fx != null)
        {
            if (instanceFx == null)
            {
                Debug.Log("fx = " + fx + " pos = " + fxPos);
                instanceFx = (GameObject)GameObject.Instantiate(fx, fxPos.transform.position, Quaternion.identity);
                instanceFx.transform.SetParent(this.transform);
            }
            instanceFx.SetActive(true);
        }
        audioSource.PlayOneShot(attackHard);
        _animation.Play(attack2);
        Invoke("EndSound", attackHard.length);
        state = E_CharacterState.E_ThirdAct;
    }
    public virtual void ResponseRaisonner()
    {
        state = E_CharacterState.E_Raisoner;
        audioSource.PlayOneShot(raisoner);
        _animation.Play(raisonner);
        ChoseNextAnimation();
        angriness += characterInfo.responseForRaisonner;
        if (angriness > characterInfo.maxAngerValue)
            angriness = characterInfo.maxAngerValue;
        else if (angriness < 0)
            angriness = 0;
        UpdateAngrinessStade();
    }
    public virtual void ResponseGueuler()
    {
        state = E_CharacterState.E_Gueler;
        audioSource.PlayOneShot(gueuler);
        _animation.Play(engueuler);
        ChoseNextAnimation();
        angriness += characterInfo.responseForGueuler;
        if (angriness > characterInfo.maxAngerValue)
            angriness = characterInfo.maxAngerValue;
        else if (angriness < 0)
            angriness = 0;
        UpdateAngrinessStade();
    }
    public virtual void ResponseClaque()
    {
        state = E_CharacterState.E_Gifle;
        int rnd = UnityEngine.Random.Range(0, randomSlap.Length);
        audioSource.PlayOneShot(randomSlap[rnd]);
        audioSource.PlayOneShot(responseGifle);
        _animation.Play(claque);
        ChoseNextAnimation();
        Invoke("EndSound", responseGifle.length);
        angriness += characterInfo.responseForClaque;
        if (angriness > characterInfo.maxAngerValue)
            angriness = characterInfo.maxAngerValue;
        else if (angriness < 0)
            angriness = 0;
        UpdateAngrinessStade();
    }
    public virtual void Ejecter()
    {
        audioSource.PlayOneShot(responseEjecter);
        Invoke("Kill", responseEjecter.length);
        isEjecting = true;
    }
    private void EndSound()
    {
        state = E_CharacterState.E_Idle;
    }
    private void Kill()
    {
        Destroy(gameObject);
    }

    public string DEBUGGetCurrentPlayingAnimationClip()
    {
        foreach (AnimationState anim in _animation)
        {
            if (_animation.IsPlaying(anim.name))
            {
                return anim.name;
            }
        }
        return string.Empty;
    }

    void Update()
    {
        //  string animName = DEBUGGetCurrentPlayingAnimationClip();
        //  if (animName != "IDLE_debout")
        //   Debug.Log("Current anim =" + animName);
        if (isEjecting == true)
        {
            Vector3 pos = transform.position;
            pos += Vector3.up * ejectSpeed * Time.deltaTime;
            transform.position = pos;
            this.transform.parent = null;
        }
        else if (isInCar == true)
        {
            timeAddAnger += Time.deltaTime;
            timeRandom += Time.deltaTime;
            CheckAddAnger();
            if (timeRandom > 1.0f)
                PlayRandomSound();
        }
        else if (isMovingToCar == true)
        {
            Transform car = GameObject.FindGameObjectWithTag("Player").transform;
            Vector3 pos = transform.position;
            if (Math.Abs(pos.z - car.position.z) >= 0.5f)
            {
                float isBehind = -1.0f;
                if (pos.z < car.position.z)
                    isBehind = 1.0f;
                pos.z += moveSpeed * isBehind * Time.deltaTime;
                transform.position = pos;
            }
        }
    }

    public void TaxiResponse()
    {
        isMovingToCar = false;
    }

    public int GetMoney()
    {
        if (angrinessStade == E_AngrinessStade.E_FirstStade)
            return characterInfo.tier1Dollars;
        else if (angrinessStade == E_AngrinessStade.E_SecondStade)
            return characterInfo.tier2Dollars;
        else if (angrinessStade == E_AngrinessStade.E_ThirdStade)
            return characterInfo.tier3Dollars;
        return 0;
    }

    private void UpdateAngrinessStade()
    {
        float oneTier = characterInfo.maxAngerValue / 3.0f;
        if (angrinessStade != E_AngrinessStade.E_FirstStade && angriness < oneTier)
        {
            ActionFirstStade();
            angrinessStade = E_AngrinessStade.E_FirstStade;
        }
        else if (angrinessStade != E_AngrinessStade.E_ThirdStade && angriness >= oneTier * 2.0f)
        {
            ActionThirdStade();
            angrinessStade = E_AngrinessStade.E_ThirdStade;
        }
        else if (angrinessStade != E_AngrinessStade.E_SecondStade && angriness >= oneTier && angriness < oneTier * 2.0f)
        {
            ActionSecondStade();
            angrinessStade = E_AngrinessStade.E_SecondStade;
        }
    }

    void PlayRandomSound()
    {
        if (state == E_CharacterState.E_Idle)
        {
            int rnd = UnityEngine.Random.Range(0, random.Length + 1);
            if (rnd < random.Length)
            {
                audioSource.PlayOneShot(random[rnd]);
                Invoke("EndSound", random[rnd].length);
                state = E_CharacterState.E_Random;
            }
            timeRandom = 0.0f;
        }
    }

    public float GetAngriness()
    {
        return angriness;
    }

    public float GetMaxAnger()
    {
        return characterInfo.maxAngerValue;
    }

    protected virtual void ChoseNextAnimation()
    {
        if (angrinessStade == E_AngrinessStade.E_FirstStade)
            _animation.PlayQueued(idleInside);
        else if (angrinessStade == E_AngrinessStade.E_SecondStade)
            _animation.PlayQueued(attack1);
        else if (angrinessStade == E_AngrinessStade.E_ThirdStade)
            _animation.PlayQueued(attack2);
    }
}
