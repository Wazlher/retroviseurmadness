﻿using UnityEngine;
using System.Collections;

public class WheelCol : MonoBehaviour
{

    private static WheelCol _instance;

    private float timeSinceCollision;
    TaxiState m_TaxiState;

    public static WheelCol instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<WheelCol>();
            return _instance;
        }
    }

    void Awake()
    {
        m_TaxiState = (TaxiState)GameObject.FindGameObjectWithTag("TaxiState").GetComponent<TaxiState>();
    }

    void OnColliderEnter()
    {
        if (timeSinceCollision >= 2.0f)
        {
            timeSinceCollision = 0.0f;
            m_TaxiState.Collision();
        }
    }

    void Update()
    {
        timeSinceCollision += Time.deltaTime;
    }
}
