﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

    public enum E_Camera
    {
        E_DRIVING,
        E_PASSENGERS,
        E_NEW_PASSENGER
    }

    public Camera DrivingCamera;
    private Animation anim;
    private E_Camera CurrentCameraType;
	public float SpeedCameraInBack;
	public float SpeedCameraOutBack;
	public float SpeedCameraInNew;
	public float SpeedCameraOutNew;

	void Start () {
        if (DrivingCamera == null)
        {
            Debug.LogError("You have to set the camera");
            return;
        }
        CurrentCameraType = E_Camera.E_DRIVING;
        anim = DrivingCamera.GetComponent<Animation>();

        DrivingCamera.enabled = true;
	}
	
	public void ChangeCamera(E_Camera newCamera)
    {
        if (newCamera == CurrentCameraType)
            return;

        if (newCamera == E_Camera.E_PASSENGERS)
        {
            anim["LookatPassengers"].speed = SpeedCameraInBack;
            anim.Play("LookatPassengers");
        }
        else if (newCamera == E_Camera.E_NEW_PASSENGER)
        {
			anim["LookNewPassenger"].speed = SpeedCameraInNew;
            anim.Play("LookNewPassenger");
        }
        else if (newCamera == E_Camera.E_DRIVING)
        {
            if (CurrentCameraType == E_Camera.E_PASSENGERS)
            {
				anim["LookatPassengers"].speed = SpeedCameraOutBack;
                anim["LookatPassengers"].time = anim["LookatPassengers"].length;
                anim.Play("LookatPassengers");
            }
            else if (CurrentCameraType == E_Camera.E_NEW_PASSENGER)
            {
				anim["LookNewPassenger"].speed = SpeedCameraOutNew;
                anim["LookNewPassenger"].time = anim["LookNewPassenger"].length;
                anim.Play("LookNewPassenger");
            }
        }

        CurrentCameraType = newCamera;

    }
}
