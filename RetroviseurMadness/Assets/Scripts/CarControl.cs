﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
	[RequireComponent(typeof (CarController))]
	public class CarControl : MonoBehaviour
	{
        public GameObject wheel;
        public float wheelRotation;

		private CarController m_Car;
        private TaxiState m_TaxiState;

		private bool accelerate;
		private bool brake;
		
		private void Awake()
		{
			m_Car = GetComponent<CarController>();
            m_TaxiState = (TaxiState)GameObject.FindGameObjectWithTag("TaxiState").GetComponent<TaxiState>();

			accelerate = false;
			brake = false;
		}
		
		
		private void FixedUpdate()
		{
			if (brake == true && m_Car.CurrentSpeed < 4.5f) 
			{
				accelerate = false;
				brake = false;
                m_TaxiState.CallPassenger();
                m_Car.StopCar();
			}

            float h = 0;
			if (m_TaxiState.GetIsEnd() == false)
                h = CrossPlatformInputManager.GetAxis("Horizontal");
			float v = 0;
			if (accelerate)
				v = 1;
			else if (brake)
				v = -1;
            m_Car.Move(h, v, v, 0);
            MoveWheel(h);
		}

        public void Accelerate()
        {
            accelerate = true;
            brake = false;
        }

        public void Brake()
        {
            brake = true;
            accelerate = false;
        }

        private void MoveWheel(float dir)
        {
            Vector3 rotation = wheel.transform.rotation.eulerAngles;
            wheel.transform.Rotate(Vector3.right, wheelRotation * -dir);
            if (dir == 0 && rotation.x > 2.0f)
            {
                if (rotation.x > 180)
                    wheel.transform.Rotate(Vector3.right, wheelRotation * 0.4f);
                else
                    wheel.transform.Rotate(Vector3.right, wheelRotation *0.4f * -1);
            }
        }
	}
}