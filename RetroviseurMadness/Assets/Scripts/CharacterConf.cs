﻿using UnityEngine;
using System.Collections;

public enum E_Character
{
    E_PRETRE,
    E_JAPONIAISE,
    E_MAITE,
    E_ALIEN,
    E_CHICKEN_MYTHIC,
    E_HOMME_AFFAIRE,
    E_LAMA
}
[System.Serializable]
public class Character
{
    public E_Character type;
    public int tier1Dollars;
    public int tier2Dollars;
    public int tier3Dollars;
    public int maxAngerValue;
    public int responseForRaisonner;
    public int responseForGueuler;
    public int responseForClaque;
    public float CoefAngerOtherCharaWhenTier3;
    public int AngerBy5Second;
    public int AngerByRoadRage;
    public AudioSource tier1Sound;
    public AudioSource tier2Sound;
    public AudioSource tier3Sound;
}

public class CharacterConf : MonoBehaviour
{

    //singleton
    private static CharacterConf _instance;
    public Character[] characters;

    public static CharacterConf instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<CharacterConf>();
            return _instance;
        }
    }

    public Character GetCharacterConf(E_Character type)
    {
        foreach (Character chara in characters)
        {
            if (chara.type == type)
                return chara;
        }
        return null;
    }

    public Character GetCharacterConf(string type)
    {
        foreach (Character chara in characters)
        {
            if (chara.type.ToString().CompareTo(type) == 0)
                return chara;
        }
        return null;
    }
}
