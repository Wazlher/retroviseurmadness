﻿using UnityEngine;
using System.Collections;

public class RoadCollider : MonoBehaviour {

	void OnTriggerExit() 
    {
        if (this.enabled)
        {
            RoadGeneration roadGeneration = (RoadGeneration)GameObject.Find("DrivingCamera").GetComponent("RoadGeneration");
            roadGeneration.DecrementNbRoad(1);
            this.enabled = false;
        }
	}
}
